package gui;

import javafx.application.Application;
import javafx.stage.Stage;

/**
 * The Main GUI class for the Application
 */
public class AppWindow extends Application{
    @Override
    public void start(Stage primaryStage) throws Exception {

    }

    public static void main(String[] args) {
        launch(args);
    }
}
