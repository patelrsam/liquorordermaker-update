package testing;

import interp.model.LiquorData;
import interp.model.Brand;
import nl.knaw.dans.common.dbflib.CorruptedTableException;
import nl.knaw.dans.common.dbflib.Table;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;


/**
 *
 */
public class backend_tester_00 {
    public static void main(String[] args){
        Scanner s = new Scanner(System.in);
//
//        System.out.println("Enter File Name: ");
        System.out.println("INITI TESTING");
        File file = new File("src/data.dbf");
        System.out.println("FILE: "+file.getTotalSpace());
        System.out.println("GOT FILE");
        Table table = new Table(file);
        System.out.println("Table Created");
        System.out.println("PRINT TABLE: " + table);
        System.out.println("TABLE SIZE: "+table.getRecordCount());


        LiquorData ld;
        try {
            ld = new LiquorData(table);
            System.out.println("Liquor Data Created");
            ArrayList<Brand> b = new ArrayList<>(ld.getAllBrands());
            System.out.println("Got Brands");
//            System.out.println(b);

            if(ld.getAllProducts().size() == table.getRecordCount()){
                System.out.println("ALL RECORDS MATCHUP!");
            }else {
                System.out.println("RECORDS NOT MATCHING");
                System.out.println("PRODUCT: "+ld.getAllProducts().size());
                System.out.println("RECORDS: "+table.getRecordCount());
            }


//            while(true){
//                s.next();
//            }
//            for (Brand bs:b){
//                System.out.println(bs);
//            }

        } catch (IOException | CorruptedTableException e) {
            e.printStackTrace();
        }


    }
}
