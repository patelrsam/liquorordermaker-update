package interp.analyze;


import interp.model.LiquorData;
import interp.model.Product;

public class Analyze {
//    private final LiquorData data;
//
//    public Analyze(LiquorData data) {
//        this.data = data;
//    }

    /*
    * How to represent data for methods like analyse info, order list smart info etc
    * should i create a package data?  or send it as
    * what else?
    * */

    public static double getAverage(Product p, int monthInterval){
        double sum = 0.0;

        for(int i=0; i < monthInterval; i++){
            sum+=p.monthSale(i);
        }
        return sum/monthInterval;
    }

    public static double getVariance(Product p, int monthInterval, double avg){
        double var = 0.0;
        for(int i = 0; i<monthInterval;i++){
            var+=Math.pow(avg-p.monthSale(i),2);
        }
        return var/monthInterval;
    }

    public static double getVariance(Product p, int monthInterval){
        double  avg = getAverage(p,monthInterval);
        return  getVariance(p,monthInterval,avg);
    }

    public  static double getStdDiv(double var){
        return Math.sqrt(var);
    }

    public static double getStdDiv(Product p, int monthInterval){
        return getStdDiv(getVariance(p,monthInterval));
    }

}
