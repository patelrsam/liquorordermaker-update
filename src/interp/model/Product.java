package interp.model;

import nl.knaw.dans.common.dbflib.Record;

import java.math.BigInteger;

/*
 * Product class packages all of the information of a particular product into one form
 */
public class Product implements Comparable<Product>, Data {
    //sale of each month,this is a circular array
    //meaning is starting at june, monthSale[0] is july's sale
    private final int[] monthSale;
    private long id;
    private long barcode; // barcode of the product
    private final String brand; //brand of the product
    private final String description; // the "flavor"/type
    private final String size; //The size of the bottle
    private final int qty; //quantity of the product in stock
    private final int mtd; // current month's sale
    private final int ytd; // this years's sale

    public Product(Record r){

        // filling current month's array with sale
        monthSale = new int[12];
        monthSale[0] = r.getNumberValue("FIRST").intValue();
        monthSale[1] = r.getNumberValue("SECON").intValue();
        monthSale[2] = r.getNumberValue("THIRD").intValue();
        monthSale[3] = r.getNumberValue("FOURT").intValue();
        monthSale[4] = r.getNumberValue("FIFTH").intValue();
        monthSale[5] = r.getNumberValue("SIXTH").intValue();
        monthSale[6] = r.getNumberValue("SEVEN").intValue();
        monthSale[7] = r.getNumberValue("EIGHT").intValue();
        monthSale[8] = r.getNumberValue("NINTH").intValue();
        monthSale[9] = r.getNumberValue("TENTH").intValue();
        monthSale[10] = r.getNumberValue("ELEVE").intValue();
        monthSale[11] = r.getNumberValue("TWELV").intValue();

        // filling all of the variables
       try{
           barcode = Long.parseLong(r.getStringValue("BARCODE").trim());
       }catch (NumberFormatException e){
           barcode = toLongHex(r.getStringValue("BARCODE").trim());
       }

        brand = r.getStringValue("BRAND").trim();
        description = r.getStringValue("DESCRIP").trim();
        size = r.getStringValue("SIZE").trim();
        qty = r.getNumberValue("QTY_ON_HND").intValue();
        mtd = r.getNumberValue("MTD").intValue();
        ytd = r.getNumberValue("YTD").intValue();
        id = getId(brand+description+size);
    }

    // changes any string to long of its hex values
    private long toLongHex(String arg) {
        return Long.parseLong(String.format("%028x", new BigInteger(1, arg.getBytes())).substring(0,16),16);
    }

    private long getId(String s){
        return barcode+s.hashCode();
    }

    //getters
    public long getBarcode() {
        return barcode;
    }

    public String getBrand() {
        return brand;
    }

    public String getDescription() {
        return description;
    }

    public String getSize() {
        return size;
    }

    public int getQty() {
        return qty;
    }

    public int getMtd() {
        return mtd;
    }

    public int getYtd() {
        return ytd;
    }

    public int monthSale(int i){
        return monthSale[i];
    }

    public long getId() {
        return id;
    }
// how to compare with? ID or Barcode?
    @Override
    public int compareTo(Product o) {
//        if(o.getId() == this.getId()) return 0;
//        else return

        return Long.compare(barcode, o.getBarcode());
    }
//    is this looking too much into it?

    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;
        if(!(obj instanceof Product)) return false;

        return id == ((Product) obj).getId() &&
                brand.equals(((Product) obj).getBrand()) &&
                description.equals(((Product) obj).getDescription()) &&
                size.equals(((Product) obj).getSize());
    }
}
