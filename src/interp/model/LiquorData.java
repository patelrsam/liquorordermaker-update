package interp.model;

/*
 * LiquorData holds all of the data of the current state of the POS
 */

import nl.knaw.dans.common.dbflib.CorruptedTableException;
import nl.knaw.dans.common.dbflib.Record;
import nl.knaw.dans.common.dbflib.Table;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeMap;

public class LiquorData {
    private HashMap<Long, Product> products;
//    HashSet<Brand> brands;
    private TreeMap<String,Brand> brands;

    public LiquorData(Table t) throws IOException, CorruptedTableException {
        // Data structure initialization

        products = new HashMap<>();
//        brands = new HashSet<>();
        brands = new TreeMap<>();
        t.open();
        int size = t.getRecordCount();
        //going through each of the records
        //current problem... some of the products have same barcode, what to do??
        for (int i = 0; i < size; i++){
            Record r = t.getRecordAt(i);
            Product p = new Product(r);
            //updating the product map
            products.put(p.getId(),p);

            //updating the brands
            //if there is none of the p's brand, we add iy
            if(!(brands.containsKey(p.getBrand()))){
                Brand b = new Brand(p.getBrand());
                b.update(p.getId());
                brands.put(p.getBrand(), b);
            }
            // else we update the existing brand with ne description
            else{
                brands.get(p.getBrand()).update(p.getId());
            }

        }
        t.close();
    }

    //getters
    public Collection<Brand> getAllBrands(){
        return brands.values();
    }

    public Brand getBrand(String b){
        return brands.get(b);
    }

    public HashSet<Long> getAllProductIds(String b){return brands.get(b).getItemNames();}

    public Product getProduct(long id){
        return products.get(id);
    }

    public Collection<Product> getAllProducts(){
        return products.values();
    }

}
