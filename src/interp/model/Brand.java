package interp.model;

import java.util.HashSet;

/**
 * Brand class holds all of the brands, and there barcodes
 */
public class Brand implements Comparable<Brand>, Data{
    private final String brand; //brand name
//    private ArrayList<String> itemNames;
    private HashSet<Long> itemNames; // long storing the barcode
    public Brand(String brandName){
        brand = brandName;
        itemNames = new HashSet<>();
    }

    // getters
    public HashSet<Long> getItemNames() {
        return itemNames;
    }

    public String getBrand() {
        return brand;
    }

    public boolean update(long item){
        return itemNames.add(item);
    }

    // returns true if the barcode exists
    public boolean has(long items){
        return itemNames.contains(items);
    }

    // new compare to, equales and toString methods
    @Override
    public int compareTo(Brand o) {
        return brand.compareTo(o.getBrand());
    }

    @Override
    public boolean equals(Object o){
        if(o == null) return false;
        if(!(o instanceof Brand)) return false;
        else{
            int cmp = brand.compareTo(((Brand) o).getBrand());
            return cmp == 0 && itemNames.size() == ((Brand) o).getItemNames().size();
//            return brand.compareTo(((Brand) o).getBrand()) == 0;
        }
    }

    @Override
    public String toString() {
        String s = brand+"\n";

        for(Long i: itemNames){
            s+="\t"+i;
        }
        return s;
    }
}
